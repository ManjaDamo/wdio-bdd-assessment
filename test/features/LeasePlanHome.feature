Feature: Lease Plan

  Scenario: Verify User can see on page filter component
    Given I navigate to LeasePlan Business Vehicle Page
    When User is in Business Vehicle Page
    And User click on Popular Filter to see options
    Then User can see onPage filter component