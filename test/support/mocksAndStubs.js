browser.addCommand('mockCarsGroup', async () => {
    const mock2 = await browser.mock('https://www.leaseplan.com/api2/cars-v3/grouped-cars?scope=business&page=1&limit=8&popularfilter=Best%20deals', {
        method: 'PUT'
    })
    const mock = await browser.mock('https://www.leaseplan.com/api2/cars-v3/grouped-cars?scope=business&page=1&limit=8&popularfilter=Configure%20yourself', {
        method: 'PUT'
    })
    await mock.respond({}, {statusCode: 500});
    await mock2.respond({}, {statusCode: 500});
    // await browser.refresh();
    // await mock.respond([{
    //     title: 'Injected (non) completed Todo',
    //     order: null,
    //     completed: false
    // }, {title: 'Injected completed Todo', order: null, completed: true}])
    // await browser.url('https://todobackend.com/client/index.html?https://todo-backend-express-knex.herokuapp.com/')
    // await (await browser.$('#todo-list li')).waitForExist()
    // console.log(await Promise.all((await browser.$$('#todo-list li')).map(el => el.getText())))// outputs: "[ 'Injected (non) completed Todo', 'Injected completed Todo' ]"
});