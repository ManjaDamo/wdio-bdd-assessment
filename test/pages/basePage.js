import commands from '../support/commands';
import mocks from '../support/mocksAndStubs';

export default class BasePage {

    open(path) {
        return browser.url(path);
    }
}
